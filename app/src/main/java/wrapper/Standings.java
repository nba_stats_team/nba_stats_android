package wrapper;

import java.util.Arrays;
import java.util.Date;

import beans.Conference;

public class Standings
{
    private Date lastUpdatedOn;
    private Conference[] conference;

    public Standings() {
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public Conference[] getConference() {
        return conference;
    }

    public void setConference(Conference[] conference) {
        this.conference = conference;
    }

    @Override
    public String toString() {
        return "Standings{" +
                "lastUpdatedOn=" + lastUpdatedOn +
                ", conference=" + Arrays.toString(conference) +
                '}';
    }
}
