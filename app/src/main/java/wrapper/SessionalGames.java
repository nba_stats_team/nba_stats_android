package wrapper;

import java.util.Arrays;
import java.util.Date;
import beans.Game;

public class SessionalGames
{
    private Date lastUpdatedOn;
    private Game[] gameScore;

    public SessionalGames() {
    }

    public Game[] getGameScore() {
        return gameScore;
    }

    public void setGameScore(Game[] gameScore) {
        this.gameScore = gameScore;
    }

    public Date getLastUpdatedOn()
    {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn)
    {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    @Override
    public String toString() {
        return "SessionalGames{" +
                "lastUpdatedOn=" + lastUpdatedOn +
                ", gameScore=" + Arrays.toString(gameScore) +
                '}';
    }
}
