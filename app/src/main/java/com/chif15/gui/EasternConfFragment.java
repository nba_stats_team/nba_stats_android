package com.chif15.gui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import beans.TeamStatistics;
import bl.AsyncDelegate;
import bl.GetJsonTask;
import bl.NetworkReceiver;
import bl.NetworkUtil;
import gui.R;

public class EasternConfFragment extends Fragment implements AsyncDelegate
{
    private ArrayAdapter<TeamStatistics> teams;
    private TableLayout standingsTable;
    private final String PULLURL = "https://api.mysportsfeeds.com/v1.2/pull/nba/2019-2020-regular/conference_team_standings.xml";
    private BroadcastReceiver receiver;
    private GetJsonTask jsonTask;

    public EasternConfFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_easternconf, container, false);

        standingsTable = view.findViewById(R.id.tlContentEasternConf);

        receiver = new NetworkReceiver();
        getActivity().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if(NetworkUtil.isConnectionAvailable(getContext()))
        {
            teams = new ArrayAdapter<TeamStatistics>(getActivity(), android.R.layout.simple_list_item_1);
            jsonTask = new GetJsonTask(teams, getActivity(), "Eastern", this);
            jsonTask.execute(PULLURL);
        }

        final SwipeRefreshLayout mSwipeRefreshLayout = view.findViewById(R.id.srlEasternConf);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        ((MainActivity) getActivity()).refreshNow();
                    }
                }
        );

        return view;
    }

    private TableRow getRow(final TeamStatistics team)
    {
        TableRow row = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_standings, null);

        ((TextView)row.findViewById(R.id.tvRank)).setText(team.getRank());
        ((TextView)row.findViewById(R.id.tvName)).setText(team.getTeam().getName());
        ((TextView)row.findViewById(R.id.tvPlayed)).setText(team.getStats().getGamesPlayed() + "");
        ((TextView)row.findViewById(R.id.tvWins)).setText(team.getStats().getWins()+"");
        ((TextView)row.findViewById(R.id.tvLosses)).setText(team.getStats().getLosses()+"");

        row.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showTeamInformation(team);
            }
        });

        return row;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void asyncComplete(boolean success)
    {
        teams.notifyDataSetChanged();

        for (int i = 0; i < teams.getCount(); i++)
        {
            standingsTable.addView(getRow(teams.getItem(i)));
        }

        GamesFragment.setTeamStats(teams);
    }

    public void showTeamInformation(TeamStatistics team)
    {
        View viewPrompt = LayoutInflater.from(getContext()).inflate(R.layout.team_information_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setView(viewPrompt);

        TextView tvTeamName = (TextView) viewPrompt.findViewById(R.id.tvTeamName);
        TextView tvPlayoffRank = (TextView) viewPrompt.findViewById(R.id.tvPlayoffAppearance);
        TextView tvGamesBack = (TextView) viewPrompt.findViewById(R.id.tvGamesBack);

        tvTeamName.setText(team.getTeam().getCity() + " " + team.getTeam().getName());

        if(Integer.parseInt(team.getRank()) > 8)
        {
            tvPlayoffRank.setText("Eliminated from Playoff Contention");
        }
        if(Integer.parseInt(team.getRank()) <= 8)
        {
            tvPlayoffRank.setText("Clinched Playoff (" + team.getRank() + ")");
        }

        tvGamesBack.setText(String.format("%.2f", team.getStats().getGamesBack()));

        String buttonClose = "<font color='#3F51B5'>Close</font>";

        builder.setCancelable(false).setPositiveButton(Html.fromHtml(buttonClose), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
