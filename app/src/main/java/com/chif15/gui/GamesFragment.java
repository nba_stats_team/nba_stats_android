package com.chif15.gui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import adapters.GamesListAdapter;
import beans.Game;
import beans.TeamStatistics;
import bl.GetJsonTask;
import bl.NetworkReceiver;
import bl.NetworkUtil;
import gui.R;

public class GamesFragment extends Fragment implements GamesListAdapter.ItemClickListener
{
    private GetJsonTask jsonTask;
    private ArrayAdapter<Spanned> lva;
    private final String PULLURL = "https://api.mysportsfeeds.com/v1.2/pull/nba/2019-2020-regular/scoreboard.xml?fordate="; //Pull Request since 12 days ago and max. 6 Entries
    private String dateStr = "";
    private BroadcastReceiver receiver;
    private static Game[] gameList = new Game[6];
    private static ArrayAdapter<TeamStatistics> teamStats;
    private RecyclerView.Adapter adapter;

    public GamesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_games, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.rv_gamesView);
        //ListView lv = view.findViewById(R.id.rv_gamesView);
        teamStats = new ArrayAdapter<TeamStatistics>(getActivity(),android.R.layout.simple_list_item_1);

        receiver = new NetworkReceiver();
        getActivity().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if(NetworkUtil.isConnectionAvailable(this.getContext()))
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date today = Calendar.getInstance().getTime();
            dateStr = dateFormat.format(today);

            adapter = new GamesListAdapter(getContext());
            //lva = new ArrayAdapter<Spanned>(getActivity(), android.R.layout.simple_list_item_1);
            jsonTask = new GetJsonTask("games", adapter, getActivity());
            jsonTask.execute(PULLURL + dateStr);

            //lv.setAdapter(lva);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            ((GamesListAdapter) adapter).setClickListener(this);

            //addGames(gameList);

            /*
            lv.setOnItemClickListener(
                    new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                        {
                            showGameDetails(gameList[i]);
                        }
                    }
            );
            */
        }

        final SwipeRefreshLayout mSwipeRefreshLayout = view.findViewById(R.id.fragment_games);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        ((MainActivity) getActivity()).refreshNow();
                    }
                }
        );

        return view;
    }

    public void addGames(Game[] games)
    {
        for (int i = 0; i < games.length; i++)
        {
            Game game = games[i];

            ((GamesListAdapter) adapter).addGame(game);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    public static void setGameList(Game[] gameList)
    {
        GamesFragment.gameList = gameList;
    }

    public static void setTeamStats(ArrayAdapter<TeamStatistics> teamList)
    {
        if(teamStats.getCount() > 0)
        {
            for (int i = 0; i < teamList.getCount(); i++)
            {
                TeamStatistics team = teamList.getItem(i);

                for (int j = 0; j < teamStats.getCount(); j++)
                {
                    if(!team.equals(teamStats.getItem(j)))
                    {
                        teamStats.add(team);
                    }
                }
            }
        }
        else
        {
            teamStats = teamList;
        }
    }

    public void showGameDetails(final Game game)
    {
        View promptView = LayoutInflater.from(getContext()).inflate(R.layout.game_information_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(promptView);

        TextView tvhomeTeam = (TextView) promptView.findViewById(R.id.tvHomeTeam);
        TextView tvawayTeam = (TextView) promptView.findViewById(R.id.tvAwayTeam);
        TextView tvScore = (TextView) promptView.findViewById(R.id.tvTotalScore);
        TextView tvVenue = (TextView) promptView.findViewById(R.id.tvVenue);
        TextView tvTime = (TextView) promptView.findViewById(R.id.tvStartTime);
        //Date d = game.getSchedule().getStartTime();
        //Calendar cal = Calendar.getInstance();
        //cal.setTime(d);
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMMM 'at' h:mm a");

        tvhomeTeam.setText(game.getHomeTeam().getCity() + " " + game.getHomeTeam().getName());
        tvawayTeam.setText(game.getAwayTeam().getCity() + " " + game.getAwayTeam().getName());
        tvScore.setText(game.getHomeScore() + " : " + game.getAwayScore());
        tvVenue.setText(game.getLocation());
        tvTime.setText(game.getDate() + " " + game.getTime());

        String buttonClose = "<font color='#3F51B5'>Close</font>";
        String buttonAddCal = "<font color='#3F51B5'>Add to Calendar</font>";

        /*builder.setCancelable(false).setPositiveButton(Html.fromHtml(buttonClose), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.cancel();
            }
        }).setNegativeButton(Html.fromHtml(buttonAddCal), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                calManager.getCalendarIntent(game);
                Toast.makeText(getContext(), "Event successfully added", Toast.LENGTH_LONG).show();
            }
        });*/

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onItemClick(View view, int position)
    {
        System.out.println("hi");
        showGameDetails(gameList[0]);
    }
}
