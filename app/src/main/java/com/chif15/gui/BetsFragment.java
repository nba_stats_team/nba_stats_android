package com.chif15.gui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import adapters.BetAdapter;
import beans.Bet;
import beans.Team;
import gui.R;

public class BetsFragment extends Fragment
{

    private BetAdapter adapter;
    private List<Bet> bets = new LinkedList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_bets, container, false);

        final SwipeRefreshLayout mSwipeRefreshLayout = view.findViewById(R.id.fragment_bets);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        ((MainActivity) getActivity()).refreshNow();
                    }
                }
        );

        RecyclerView recyclerView = view.findViewById(R.id.rv_bets);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        adapter = BetAdapter.getInstance();
        test();
        adapter.setList(bets);
        adapter.setInflater(LayoutInflater.from(view.getContext()));
        recyclerView.setAdapter(adapter);

        return view;
    }

    public void test()
    {
        Bet bet = new Bet(new Team("New York Nicks",1,"NYK","New York"),new Team("Golden State Warriors",1,"GSW","San Francisco"),0.2,0.2,100, Calendar.getInstance().getTime());
        Bet bet1 = new Bet(new Team("New York Nicks",1,"NYK","New York"),new Team("Golden State Warriors",1,"GSW","San Francisco"),0.2,0.2,100, Calendar.getInstance().getTime());
        Bet bet2 = new Bet(new Team("New York Nicks",1,"NYK","New York"),new Team("Golden State Warriors",1,"GSW","San Francisco"),0.2,0.2,100, Calendar.getInstance().getTime());
        Bet bet3 = new Bet(new Team("New York Nicks",1,"NYK","New York"),new Team("Golden State Warriors",1,"GSW","San Francisco"),0.2,0.2,100, Calendar.getInstance().getTime());

        bets.add(bet);
        bets.add(bet1);
        bets.add(bet2);
        bets.add(bet3);



    }
}
