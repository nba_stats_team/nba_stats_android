package com.chif15.gui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;

import adapters.ViewPagerAdapter;
import gui.R;

public class ConferenceFragment extends Fragment
{
    TabLayout tabLayout;
    ViewPager viewPagerConfs;
    ViewPagerAdapter confPagerAdapter;

    public ConferenceFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_conference, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.child_tabs);
        viewPagerConfs = (ViewPager) view.findViewById(R.id.childViewPager);

        setupViewPager(viewPagerConfs);
        tabLayout.setupWithViewPager(viewPagerConfs);
        return view;
    }

    private void setupViewPager(ViewPager viewPager)
    {
        confPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        confPagerAdapter.addFragments(new EasternConfFragment(), "Eastern Conference");
        confPagerAdapter.addFragments(new WesternConfFragment(), "Western Conference");
        viewPager.setAdapter(confPagerAdapter);
    }
}
