package com.chif15.gui;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import gui.R;

public class MainActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private BottomNavigationView navigationView;
    final Fragment fragment1 = new GamesFragment();
    final Fragment fragment2 = new ConferenceFragment();
    final Fragment fragment3 = new BetsFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        fm.beginTransaction().add(R.id.fragment_container, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.fragment_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.fragment_container, fragment1, "1").commit();

        navigationView = findViewById(R.id.bottom_navigation);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.menu_games:
                        fm.beginTransaction().hide(active).show(fragment1).commit();
                        active = fragment1;

                        return true;
                    case R.id.menu_conferences:
                        fm.beginTransaction().hide(active).show(fragment2).commit();
                        active = fragment2;

                        return true;
                    case R.id.menu_bets:
                        fm.beginTransaction().hide(active).show(fragment3).commit();
                        active = fragment3;

                        return true;
                }

                return false;
            }
        });
    }

    public void refreshNow()
    {
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }
}
