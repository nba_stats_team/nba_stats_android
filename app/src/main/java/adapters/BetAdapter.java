package adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import beans.Bet;
import gui.R;

public class BetAdapter extends RecyclerView.Adapter<BetAdapter.ViewHolder>
{

    private List<Bet> betlist = new LinkedList<>();
    private LayoutInflater inflater;
    private static BetAdapter betInstance;

    private BetAdapter() {}

    public static BetAdapter getInstance()
    {
        if(betInstance == null)
        {
            betInstance = new BetAdapter();
        }
        return betInstance;
    }

    public void setInflater(LayoutInflater inflater)
    {
        this.inflater = inflater;
    }

    public void setList(List list)
    {
        betlist.addAll(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.fragment_bets_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Bet bet = betlist.get(i);
        viewHolder.team1.setText(bet.getTeam1().getAbbreviation());
        viewHolder.team2.setText(bet.getTeam2().getAbbreviation());
        viewHolder.status.setText("Upcoming");

        SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, dd.MM.yyyy");

        String date = dateFormatter.format(bet.getDate());

        viewHolder.date.setText(date);
    }

    @Override
    public int getItemCount() {
        return betlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView team1;
        TextView team2;
        TextView status;
        TextView date;
        TextView bet;
        CardView cv;

        ViewHolder(View itemView)
        {
            super(itemView);
            date = itemView.findViewById(R.id.tv_date);
            cv = itemView.findViewById(R.id.cv);
            team1 = itemView.findViewById(R.id.tv_team1);
            team2 = itemView.findViewById(R.id.tv_team2);
            status = itemView.findViewById(R.id.tv_status);
            bet = itemView.findViewById(R.id.tv_bet);
        }


    }


}
