package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import beans.Game;
import beans.Team;
import gui.R;

public class GamesListAdapter extends RecyclerView.Adapter<GamesListAdapter.ViewHolder>
{
    private ArrayList<Game> gamesList = new ArrayList<>();
    private Context context;
    private ItemClickListener gameClickListener;

    public GamesListAdapter(Context context)
    {
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public ImageView teamLogo1;
        public ImageView teamLogo2;

        public TextView team1;
        public TextView team2;
        public TextView finalScore1;
        public TextView finalScore2;
        public TextView startTime;
        public TextView venue;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);

            this.teamLogo1 = itemView.findViewById(R.id.iv_team1);
            this.teamLogo2 = itemView.findViewById(R.id.iv_team2);

            this.team1 = itemView.findViewById(R.id.tv_team1);
            this.team2 = itemView.findViewById(R.id.tv_team2);
            this.finalScore1 = itemView.findViewById(R.id.tv_finalScore1);
            this.finalScore2 = itemView.findViewById(R.id.tv_finalScore2);
            this.startTime = itemView.findViewById(R.id.tv_startTime);
            this.venue = itemView.findViewById(R.id.tv_venue);
        }

        @Override
        public void onClick(View view)
        {
            gameClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_games_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition)
    {
        //ImageView image = gamesList.get(listPosition).getHomeTeam();
        //ColorFilter cf = image.getColorFilter();

        //holder.projectImage.setColorFilter(cf);

        holder.teamLogo1.setImageResource(getTeamIcon(gamesList.get(listPosition).getHomeTeam()));
        holder.teamLogo2.setImageResource(getTeamIcon(gamesList.get(listPosition).getAwayTeam()));

        holder.team1.setText(gamesList.get(listPosition).getHomeTeam().getCity() + " " + gamesList.get(listPosition).getHomeTeam().getName());
        holder.team2.setText(gamesList.get(listPosition).getAwayTeam().getCity() + " " + gamesList.get(listPosition).getAwayTeam().getName());
        holder.finalScore1.setText(gamesList.get(listPosition).getHomeScore() == 0 ? "-" : gamesList.get(listPosition).getHomeScore() + "");
        holder.finalScore2.setText(gamesList.get(listPosition).getAwayScore() == 0 ? "-" : gamesList.get(listPosition).getAwayScore() + "");
        holder.startTime.setText(gamesList.get(listPosition).getDate() + " " + gamesList.get(listPosition).getTime());
        holder.venue.setText(gamesList.get(listPosition).getLocation());
    }

    @Override
    public int getItemCount()
    {
        return gamesList.size();
    }

    public Game getItem(int id)
    {
        return gamesList.get(id);
    }

    public int getTeamIcon(Team team)
    {
        switch(team.getAbbreviation())
        {
            case "ATL": return R.mipmap.hawks;
            case "BOS": return R.mipmap.celtics;
            case "BKN": return R.mipmap.nets;
            case "CHA": return R.mipmap.hornets;
            case "CHI": return R.mipmap.bulls;
            case "CLE": return R.mipmap.cavs;
            case "DAL": return R.mipmap.mavs;
            case "DEN": return R.mipmap.nuggets;
            case "DET": return R.mipmap.pistons;
            case "GSW": return R.mipmap.warriors;
            case "HOU": return R.mipmap.rockets;
            case "IND": return R.mipmap.pacers;
            case "LAC": return R.mipmap.clippers;
            case "LAL": return R.mipmap.lakers;
            case "MEM": return R.mipmap.grizzlies;
            case "MIA": return R.mipmap.heat;
            case "MIL": return R.mipmap.bucks;
            case "MIN": return R.mipmap.timber;
            case "NOP": return R.mipmap.pelicans;
            case "NYK": return R.mipmap.knicks;
            case "OKC": return R.mipmap.thunder;
            case "ORL": return R.mipmap.magic;
            case "PHI": return R.mipmap.sixers;
            case "PHX": return R.mipmap.suns;
            case "POR": return R.mipmap.portland;
            case "SAC": return R.mipmap.kings;
            case "SAS": return R.mipmap.spurs;
            case "TOR": return R.mipmap.raptors;
            case "UTA": return R.mipmap.jazz;
            case "WAS": return R.mipmap.wizards;
            default: return R.mipmap.basketball_icon;
        }
    }

    public void addGame(Game game)
    {
        /*
        game.setHomeTeamImage(new ImageView(context));
        game.setHomeTeamImageID(R.drawable.team);
        game.getHomeTeamImage().setImageResource(R.drawable.team);
        game.setAwayTeamImage(new ImageView(context));
        game.setAwayTeamImageID(R.drawable.team);
        game.getAwayTeamImage().setImageResource(R.drawable.team);
        */

        gamesList.add(game);

        notifyDataSetChanged();
    }

    public void setClickListener(GamesListAdapter.ItemClickListener ItemClickListener)
    {
        this.gameClickListener = ItemClickListener;
    }

    public interface ItemClickListener
    {
        void onItemClick(View view, int position);
    }
}
