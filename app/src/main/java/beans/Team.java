package beans;

public class Team
{
    private String name;
    private int id;
    private String abbreviation;
    private String city;

    public Team() {
    }

    public Team(String name, int id, String abbreviation, String city) {
        this.name = name;
        this.id = id;
        this.abbreviation = abbreviation;
        this.city = city;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    @Override
    public String toString()
    {
        return "Team{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", abbreviation='" + abbreviation + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
