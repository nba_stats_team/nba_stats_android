package beans;

public class TeamStatistics
{
    private Team team;
    private String rank;
    private Stats stats;

    public TeamStatistics() {
    }

    public Team getTeam()
    {
        return team;
    }

    public void setTeam(Team team)
    {
        this.team = team;
    }

    public Stats getStats()
    {
        return stats;
    }

    public void setStats(Stats stats)
    {
        this.stats = stats;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "TeamStatistics{" +
                "team=" + team +
                ", rank='" + rank + '\'' +
                ", stats=" + stats +
                '}';
    }
}
