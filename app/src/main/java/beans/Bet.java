package beans;

import java.util.Calendar;
import java.util.Date;

public class Bet {

    private Team team1;
    private Team team2;
    private double quote1;
    private double quote2;
    private double stake;
    private Date date;

    public Bet() {
    }

    public Bet(Team team1, Team team2, double quote1, double quote2, double stake, Date date) {
        this.team1 = team1;
        this.team2 = team2;
        this.quote1 = quote1;
        this.quote2 = quote2;
        this.stake = stake;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }



    public void setDate(Date date) {
        this.date = date;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public double getQuote1() {
        return quote1;
    }

    public void setQuote1(double quote1) {
        this.quote1 = quote1;
    }

    public double getQuote2() {
        return quote2;
    }

    public void setQuote2(double quote2) {
        this.quote2 = quote2;
    }

    public double getStake() {
        return stake;
    }

    public void setStake(double stake) {
        this.stake = stake;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "team1=" + team1 +
                ", team2=" + team2 +
                ", quote1=" + quote1 +
                ", quote2=" + quote2 +
                ", stake=" + stake +
                ", date=" + date +
                '}';
    }
}
