package beans;

import java.util.Arrays;

public class Conference
{
    private TeamStatistics[] teamentry;

    public Conference() {
    }

    public TeamStatistics[] getTeamentry() {
        return teamentry;
    }

    public void setTeamentry(TeamStatistics[] teamentry) {
        this.teamentry = teamentry;
    }

    @Override
    public String toString() {
        return "Conference{" +
                "teamentry=" + Arrays.toString(teamentry) +
                '}';
    }
}
