package beans;

public class Stats
{
    private int gamesPlayed;
    private int wins;
    private int losses;
    private Double gamesBack;

    public Stats() {
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public Double getGamesBack() {
        return gamesBack;
    }

    public void setGamesBack(Double gamesBack) {
        this.gamesBack = gamesBack;
    }

    @Override
    public String toString() {
        return "Stats{" +
                "gamesPlayed=" + gamesPlayed +
                ", wins=" + wins +
                ", losses=" + losses +
                ", gamesBack=" + gamesBack +
                '}';
    }
}
