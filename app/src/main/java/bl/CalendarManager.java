package bl;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import beans.Game;
import beans.MyCalendar;

/**
 * Class for creating interface to local Calendar
 *
 * @author swamac15
 * @version 2019/06/12
 * @since 2019/06/12
 */
public class CalendarManager
{
    ContentResolver cr;
    ContentValues event;
    Context mContext;

    public CalendarManager(Context mContext) {
        this.mContext = mContext;
        cr = mContext.getContentResolver();
        event = new ContentValues();
    }

    /**
     * Method that creates new event in Local Calendar
     *
     * @param game
     */
    public void getCalendarIntent(Game game)
    {
        /*
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");

        Calendar startTime = Calendar.getInstance();
        startTime.setTime(game.getSchedule().getStartTime());
        long startTimeLong = startTime.getTimeInMillis(); //Converts the starttime to milliseconds -> better for usage

        Calendar endTime = Calendar.getInstance();
        endTime.setTime(game.getSchedule().getStartTime());
        endTime.add(Calendar.HOUR_OF_DAY, 2);
        long endTimeLong = endTime.getTimeInMillis(); //Converts the endtime to milliseconds -> better for usage

        event.put("calendar_id", getCalendarID()); //Sets the Calendar ID
        event.put("title", game.getSchedule().getHomeTeam().getAbbreviation() + " vs. " + game.getSchedule().getAwayTeam().getAbbreviation()); //Sets the Title for the calendar event
        event.put("description", game.getSchedule().getHomeTeam().getAbbreviation() + " vs. " + game.getSchedule().getAwayTeam().getAbbreviation() + "\n" +
                "Venue: " + game.getSchedule().getVenue().getName()); //Sets description for event
        event.put("eventLocation", game.getSchedule().getVenue().getName()); //Sets the location for event
        event.put("eventTimezone", TimeZone.getDefault().getID()); //Sets timezone
        event.put("dtstart", startTimeLong); //Sets the start of event
        event.put("dtend", endTimeLong); //Sets the end of event
        event.put("allDay", 0); //0 if not all day, 1 for all-day event
        event.put("eventStatus", 1);
        event.put("hasAlarm", 1); //Sets alarm for event

        String eventUriStr = "content://com.android.calendar/events";
        Uri eventUri = mContext.getApplicationContext().getContentResolver().insert(Uri.parse(eventUriStr), event); //Saves event to calendar
        long eventID = Long.parseLong(eventUri.getLastPathSegment()); //Gathers event id for later usage
        //Creates the additional reminder with 12h earlier
        int min = 720;
        ContentValues reminder = new ContentValues();
        reminder.put("event_id", eventID); //Sets event id to reminder for association
        reminder.put("method", "1");
        reminder.put("minutes", min); //Sets the time between reminder and event

        String reminderUriStr = "content://com.android.calendar/reminders";
        mContext.getApplicationContext().getContentResolver().insert(Uri.parse(reminderUriStr), reminder);

         */
    }

    /**
     * This method returns the first Calendar in particular phone
     *
     * @return Specific int which is the Calendar ID of the first occured calendar
     */
    public int getCalendarID() {
        String projection[] = {"_id", "calendar_displayName"};
        Uri calendars;
        MyCalendar[] cals = null;
        calendars = Uri.parse("content://com.android.calendar/calendars");

        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor managedCursor = contentResolver.query(calendars, projection, null, null, null); //Creates cursor for all calendars

        if (managedCursor.moveToFirst()){ //Iterates through all calendars on device
            cals = new MyCalendar[managedCursor.getCount()];
            String calName;
            int calID;
            int cont= 0;
            int nameCol = managedCursor.getColumnIndex(projection[1]);
            int idCol = managedCursor.getColumnIndex(projection[0]);
            do {
                calName = managedCursor.getString(nameCol);
                calID = Integer.parseInt(managedCursor.getString(idCol));
                cals[cont] = new MyCalendar(calName, calID); //Stores MyCalendar onto cals
                cont++;
            } while(managedCursor.moveToNext());
            managedCursor.close();
        }
        return cals[0].getId(); //returns first calendar ID on device
    }
}
