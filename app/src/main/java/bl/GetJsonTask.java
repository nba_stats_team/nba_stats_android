package bl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.ArrayAdapter;

import androidx.recyclerview.widget.RecyclerView;

import com.chif15.gui.GamesFragment;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import adapters.GamesListAdapter;
import beans.Conference;
import beans.Game;
import beans.Stats;
import beans.Team;
import beans.TeamStatistics;
import gui.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import wrapper.SessionalGames;
import wrapper.Standings;

/**
 * Class extends AsyncTask in order to not run on main thread
 * Helps to gather and model the JSON information
 *
 * @author swamac15
 * @version 2019/06/09
 * @since 2019/05/06
 */
public class GetJsonTask extends AsyncTask<String,Void,Object>
{
    private final String TOKEN = "79b0b7b2-06fb-4ff2-9eca-f9e1d5";
    private final String PASSWORD = "Matthaeus09";
    private String pullType = "";
    private RecyclerView.Adapter adapter;
    private ArrayAdapter<Spanned> lva;
    private ArrayAdapter<TeamStatistics> teams;
    private ProgressDialog dialog;
    private Activity activity;
    private String translation;
    private AsyncDelegate delegate;


    public GetJsonTask(String translation, RecyclerView.Adapter adapter, Activity activity) //Constructor for ArrayAdapter Games
    {
        this.adapter = adapter;
        this.activity = activity;
        this.translation = translation;
        dialog = new ProgressDialog(activity, R.style.DialogStyle);
    }

    public GetJsonTask(ArrayAdapter<TeamStatistics> teams, Activity activity, String translation, AsyncDelegate delegate) { //Constructor for ArrayAdapter Tables Standings
        this.teams = teams;
        this.activity = activity;
        this.translation = translation;
        this.delegate = delegate;
        dialog = new ProgressDialog(activity, R.style.DialogStyle);
    }

    /**
     * This method converts the JSON Response String to specific Wrapper Class
     *
     * @param content JSON Response String
     * @param pullType Type of Pull Request (standings, games)
     * @return Specific data object of particular Wrapper class (Standings, SessionalGames)
     */
    public Document convertResponseToDocument(InputStream content, String pullType) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document dom = builder.parse(content);

        System.out.println("TAG NAME: " + dom.getDocumentElement().getTagName());

        return dom;
    }

    @Override
    protected void onPreExecute()
    {
        dialog.setMessage("Fetch data...");
        dialog.show(); //Starts dialog -> disables interactions with app during that time
    }

    /**
     *
     * @param urls The PULLURL defined in Activities
     * @return Response of JSON Request as Object
     */
    @Override
    protected Object doInBackground(String... urls)
    {
        try {
            Object response = null;
            String parts[] = urls[0].split("/");
            Log.e("TASK",urls[0]);
            String newParts[] = parts[7].split("\\.");
            pullType = newParts[0]; //Sets pull type
            Log.e("Pulltype", pullType);

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            Request request = new Request.Builder()
                    .url(urls[0])
                    .method("GET", null)
                    .addHeader("Authorization", "Basic NzliMGI3YjItMDZmYi00ZmYyLTllY2EtZjllMWQ1Ok1hdHRoYWV1czA5")
                    .build();
            Response responseOk = client.newCall(request).execute();

            InputStream stream = responseOk.body().byteStream();

            Log.e("doInBackground", stream.available() + "");

            response = convertResponseToDocument(stream, pullType); //Converts JSON Response to PUllType and stores it onto response

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method will be called after doInBackground and will process the Response in order to convert it to proper Wrapper
     *
     * @param o The response of JSON Request processed in doInBackground
     */
    @Override
    protected void onPostExecute(Object o) {

        if(pullType.equals("scoreboard"))
        {
            Document dom = (Document) o;
            Log.e("onPostExecute", ((Document) o).getDocumentElement().getChildNodes().item(0).getTextContent());

            SessionalGames games = new SessionalGames();

            NodeList gameScore = dom.getElementsByTagName("scor:gameScore");

            Game[] gameArray = new Game[gameScore.getLength()];

            for (int i = 0; i < gameScore.getLength(); i++) {
                NodeList gameData = gameScore.item(i).getChildNodes();

                Game game = new Game();
                Team away = new Team();
                Team home = new Team();

                away.setId(Integer.parseInt(gameData.item(0).getChildNodes().item(7).getChildNodes().item(0).getTextContent()));
                away.setCity(gameData.item(0).getChildNodes().item(7).getChildNodes().item(1).getTextContent());
                away.setName(gameData.item(0).getChildNodes().item(7).getChildNodes().item(2).getTextContent());
                away.setAbbreviation(gameData.item(0).getChildNodes().item(7).getChildNodes().item(3).getTextContent());

                home.setId(Integer.parseInt(gameData.item(0).getChildNodes().item(8).getChildNodes().item(0).getTextContent()));
                home.setCity(gameData.item(0).getChildNodes().item(8).getChildNodes().item(1).getTextContent());
                home.setName(gameData.item(0).getChildNodes().item(8).getChildNodes().item(2).getTextContent());
                home.setAbbreviation(gameData.item(0).getChildNodes().item(8).getChildNodes().item(3).getTextContent());

                //TODO: bro do passt wos mitn node net
                //game.setAwayScore(120);
                //game.setHomeScore(123);
                if(gameData.getLength() > 5)
                {
                    game.setAwayScore(Integer.parseInt(gameData.item(4).getTextContent()));
                    game.setHomeScore(Integer.parseInt(gameData.item(5).getTextContent()));
                }else
                {
                    game.setAwayScore(0);
                    game.setHomeScore(0);
                }

                game.setCompleted(gameData.item(3).getTextContent().equals("true") ? true : false);
                game.setTime(gameData.item(0).getChildNodes().item(6).getTextContent());
                game.setDate(gameData.item(0).getChildNodes().item(5).getTextContent());
                game.setLocation(gameData.item(0).getChildNodes().item(9).getTextContent());

                game.setAwayTeam(away);
                game.setHomeTeam(home);

                gameArray[i] = game;
                ((GamesListAdapter) adapter).addGame(game);
            }
            games.setGameScore(gameArray);

            Log.e("onPostExecute", games.getGameScore().length + "");

            /*
            String[] unformattedList = getGamesList(games); //Creates HTML formatted content onto String Array
            Spanned[] htmlFormattedList = new Spanned[unformattedList.length];

            for (int i = 0; i < unformattedList.length; i++) {
                htmlFormattedList[i] = Html.fromHtml(unformattedList[i]); //Converts HTML String to Spanned Object (Displays HTML)
            }

            lva.addAll(htmlFormattedList); //Adds HTML Content to ArrayAdapter of GamesFragment
            */
        }

        if(pullType.equals("conference_team_standings"))
        {
            Document dom = (Document) o;
            Log.e("onPostExecute", ((Document) o).getDocumentElement().getChildNodes().item(0).getTextContent());

            Standings standings = new Standings();

            Conference eastern = new Conference();
            Conference western = new Conference();

            NodeList conferences = dom.getElementsByTagName("con:conference");

            NodeList easternEntries = conferences.item(0).getChildNodes();
            TeamStatistics[] easternEntriesArray = new TeamStatistics[easternEntries.getLength()];

            NodeList westernEntries = conferences.item(1).getChildNodes();
            TeamStatistics[] westernEntriesArray = new TeamStatistics[westernEntries.getLength()];

            for (int i = 0; i < easternEntries.getLength(); i++) {
                TeamStatistics stats = new TeamStatistics();

                Team team = new Team();
                Stats teamStats = new Stats();

                team.setId(Integer.parseInt(easternEntries.item(i).getChildNodes().item(0).getChildNodes().item(0).getTextContent()));
                team.setCity(easternEntries.item(i).getChildNodes().item(0).getChildNodes().item(1).getTextContent());
                team.setName(easternEntries.item(i).getChildNodes().item(0).getChildNodes().item(2).getTextContent());
                team.setAbbreviation(easternEntries.item(i).getChildNodes().item(0).getChildNodes().item(3).getTextContent());

                teamStats.setGamesPlayed(Integer.parseInt(easternEntries.item(i).getChildNodes().item(2).getChildNodes().item(0).getTextContent()));
                teamStats.setWins(Integer.parseInt(easternEntries.item(i).getChildNodes().item(2).getChildNodes().item(64).getTextContent()));
                teamStats.setLosses(Integer.parseInt(easternEntries.item(i).getChildNodes().item(2).getChildNodes().item(65).getTextContent()));
                teamStats.setGamesBack(Double.parseDouble(easternEntries.item(i).getChildNodes().item(2).getChildNodes().item(67).getTextContent()));

                stats.setRank(easternEntries.item(i).getChildNodes().item(1).getTextContent());
                stats.setTeam(team);
                stats.setStats(teamStats);
                easternEntriesArray[i] = stats;
            }

            for (int i = 0; i < easternEntries.getLength(); i++) {
                TeamStatistics stats = new TeamStatistics();

                Team team = new Team();
                Stats teamStats = new Stats();

                team.setId(Integer.parseInt(westernEntries.item(i).getChildNodes().item(0).getChildNodes().item(0).getTextContent()));
                team.setCity(westernEntries.item(i).getChildNodes().item(0).getChildNodes().item(1).getTextContent());
                team.setName(westernEntries.item(i).getChildNodes().item(0).getChildNodes().item(2).getTextContent());
                team.setAbbreviation(westernEntries.item(i).getChildNodes().item(0).getChildNodes().item(3).getTextContent());

                teamStats.setGamesPlayed(Integer.parseInt(westernEntries.item(i).getChildNodes().item(2).getChildNodes().item(0).getTextContent()));
                teamStats.setWins(Integer.parseInt(westernEntries.item(i).getChildNodes().item(2).getChildNodes().item(64).getTextContent()));
                teamStats.setLosses(Integer.parseInt(westernEntries.item(i).getChildNodes().item(2).getChildNodes().item(65).getTextContent()));
                teamStats.setGamesBack(Double.parseDouble(westernEntries.item(i).getChildNodes().item(2).getChildNodes().item(67).getTextContent()));

                stats.setRank(westernEntries.item(i).getChildNodes().item(1).getTextContent());
                stats.setTeam(team);
                stats.setStats(teamStats);
                westernEntriesArray[i] = stats;
            }

            eastern.setTeamentry(easternEntriesArray);
            western.setTeamentry(westernEntriesArray);

            standings.setConference(new Conference[]{eastern, western});

            teams.clear();
            teams.addAll(getStandingsList(standings));
            delegate.asyncComplete(true);
        }

        if(dialog.isShowing())
        {
            dialog.dismiss(); //Dismiss dialog -> Interaction with app is now allowed
        }

    }

    /**
     * This method converts the SessionalGames Object to formatted String and adds it to Array
     *
     * @param gamesObj Object of SessionalGames
     * @return String Array of all Games
     */
    public String[] getGamesList(SessionalGames gamesObj)
    {
        Game[] allGames = gamesObj.getGameScore();
        String[] gameList = new String[allGames.length];
        Log.e("getGamesList", allGames.length + "");
        GamesFragment.setGameList(allGames);
        for(int i = 0; i < allGames.length; i++)
        {
            String awayTeam = allGames[i].getAwayTeam().getAbbreviation();
            String homeTeam = allGames[i].getHomeTeam().getAbbreviation();
            int awayScore = allGames[i].getAwayScore();
            int homeScore = allGames[i].getHomeScore();
            gameList[i] = String.format("<b>%s</b> - %d<br>" +
                                        "<b>%s</b> - %d", awayTeam, awayScore, homeTeam, homeScore); //Stores information to HTML String
        }

        return gameList;
    }

    /**
     * This method converts the Standings Object and adds it to Array
     *
     * @param standingsObj Object of Standings
     * @return TeamStatistics Array with all teams
     */
    public TeamStatistics[] getStandingsList(Standings standingsObj)
    {
        Log.e("getStandingsList", standingsObj.toString());
        int anz = 0;
        TeamStatistics[] teamStats = null;
        if(translation.equals("Western"))
        {
            teamStats = standingsObj.getConference()[1].getTeamentry();
        }else
        {
            teamStats = standingsObj.getConference()[0].getTeamentry();
        }

        for (TeamStatistics stats : teamStats) {
            anz++;
        }
        TeamStatistics[] standingsList = new TeamStatistics[anz];

        for (TeamStatistics stats : teamStats) {
            int confRank = Integer.parseInt(stats.getRank());
            standingsList[confRank-1] = stats; //Add Item in array (Ordered)
        }
        return standingsList;
    }
}
