package bl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This class serves as a Util class which helps to identify whether network connectivity is available or not
 *
 * @author swamac15
 * @version 2019/05/28
 * @since 2019/05/28
 */
public class NetworkUtil
{
    /**
     * This method checks the Network State
     *
     * @param context The context of specific activity
     * @return True or false whether connectivity is available or not
     */
    public static boolean isConnectionAvailable(Context context)
    {
        ConnectivityManager connMngr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connMngr.getActiveNetworkInfo();

        if(activeNetwork != null)
        {
            switch(activeNetwork.getType())
            {
                case ConnectivityManager.TYPE_MOBILE: return true; //If Connection is available (Mobile) return true
                case ConnectivityManager.TYPE_WIFI: return true; //If Connection is available (Wifi) return true
                default: return false; //If no connection is type Mobile or Wifi -> Return false
            }
        }
        return false;
    }
}
