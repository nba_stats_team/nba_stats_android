package bl;
/**
 * Interface which helps to identify a complete task
 *
 * @author swamac15
 * @version 2019/06/08
 * @since 2019/06/08
 */
public interface AsyncDelegate
{
    public void asyncComplete(boolean success);
}
