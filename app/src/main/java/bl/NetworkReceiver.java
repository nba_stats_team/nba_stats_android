package bl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * This class is used to work as a Receiver which checks if connectivity is available and if not it will show a Toast
 * 
 * @author swamac15
 * @version 2019/05/28
 * @since 2019/05/28
 */
public class NetworkReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    { //Called every Connectiviy Action
        if(!NetworkUtil.isConnectionAvailable(context))
        {
            Toast.makeText(context, "No Connection available...", Toast.LENGTH_SHORT).show(); //Shows toast that no connection is available
        }
    }
}
